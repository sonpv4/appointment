package com.fhir.demo.service;

import java.util.List;

import com.fhir.demo.dto.UserDTO;
import org.keycloak.representations.idm.UserRepresentation;

import com.fhir.demo.business.UserBO;

public interface KeycloakAdminClientService {

	void addUser(UserDTO user);

	List<UserRepresentation> getAllUsers();
}
