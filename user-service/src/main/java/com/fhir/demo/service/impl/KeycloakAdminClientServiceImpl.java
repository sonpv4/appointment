package com.fhir.demo.service.impl;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.ws.rs.core.Response;

import com.fhir.demo.config.KeycloakProvider;
import com.fhir.demo.dto.UserDTO;
import org.keycloak.admin.client.CreatedResponseUtil;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fhir.demo.service.KeycloakAdminClientService;

@Service("keycloakAdminClientService")
public class KeycloakAdminClientServiceImpl implements KeycloakAdminClientService {

    @Autowired
    private KeycloakProvider keycloakProvider;

    @Override
    public void addUser(final UserDTO user) {
        // Get realm
        RealmResource realmResource = keycloakProvider.getInstance().realm(keycloakProvider.getRealm());
        UsersResource usersResource = realmResource.users();
        CredentialRepresentation credentialRepresentation = createPasswordCredentials(user.getPassword());

        // Define user
        UserRepresentation kcUser = new UserRepresentation();
        kcUser.setUsername(user.getUsername());
        kcUser.setCredentials(Collections.singletonList(credentialRepresentation));
        kcUser.setFirstName(user.getFirstName());
        kcUser.setLastName(user.getLastName());
        kcUser.setEnabled(true);
        kcUser.setEmail("test@localhost.com");
        kcUser.setEmailVerified(true);

        // Create user
        Response response = usersResource.create(kcUser);
        System.out.printf("Repsonse: %s %s%n", response.getStatus(), response.getStatusInfo());
        System.out.println(response.getLocation());
        String userId = CreatedResponseUtil.getCreatedId(response);
        usersResource.create(kcUser);

        System.out.printf("User created with userId: %s%n", userId);

        // assign user role
        RoleRepresentation userRole = realmResource.roles().get("user").toRepresentation();
        usersResource.get(userId).roles().realmLevel().add(Arrays.asList(userRole));
    }

    @Override
    public List<UserRepresentation> getAllUsers() {
        return keycloakProvider.getInstance().realm(keycloakProvider.getRealm()).users().list();
    }

    private static CredentialRepresentation createPasswordCredentials(final String password) {
        CredentialRepresentation passwordCredentials = new CredentialRepresentation();
        passwordCredentials.setTemporary(false);
        passwordCredentials.setType(CredentialRepresentation.PASSWORD);
        passwordCredentials.setValue(password);
        return passwordCredentials;
    }
}
