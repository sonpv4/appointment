package com.fhir.demo.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@ToString
public class UserDTO {
	private String username;
	private String firstName;
	private String lastName;
	private String password;
}
