package com.fhir.demo.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.fhir.demo.business.UserBO;
import com.fhir.demo.dto.UserDTO;

@Mapper
public interface UserMapper {
	UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

	UserBO toBusinessObject(UserDTO source);
}


