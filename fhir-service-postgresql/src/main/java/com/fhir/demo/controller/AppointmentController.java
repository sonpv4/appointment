package com.fhir.demo.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fhir.demo.service.AppointmentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import com.fhir.demo.dto.AppointmentDto;
import com.fhir.demo.mapper.AppointmentMapper;
import com.fhir.demo.model.Appointment;

import javax.validation.Valid;

@RestController
@RequestMapping("/appointment")
public class AppointmentController {

    private final AppointmentService appointmentService;
    private final AppointmentMapper mapper = AppointmentMapper.INSTANCE;

    public AppointmentController(AppointmentService appointmentService) {
        this.appointmentService = appointmentService;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Appointment createAppointment(@Valid @RequestBody Appointment appointment) {
        return appointmentService.create(appointment);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Appointment findOne(@PathVariable final String id) {
        return appointmentService.lookUpByAppointmentId(id);
    }

    @GetMapping(value = "/{id}/_history/{versionId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Appointment history(@PathVariable final String id, @PathVariable final String versionId) {
        Appointment appointment = appointmentService.lookup(id, versionId);

        return appointment;
    }

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Appointment update(@PathVariable final String id, @RequestBody final Appointment appointment) {
        appointment.setId(id);

        return appointmentService.update(appointment);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Appointment> findAll() {
        List<Appointment> appointments = appointmentService.listAll();

        return appointments;
    }

    /**
     * Lookup appointments
     *
     * @param actor            - Any one of the individuals participating in the appointment
     * @param appointmentType  - The style of appointment or patient that has been booked in the slot (not service type)
     * @param date             - Appointment date/time
     * @param identifier       - An Identifier of the Appointment
     * @param incomingReferral - The ReferralRequest provided as information to allocate to the Encounter
     * @param location         - This location is listed in the participants of the appointment
     * @param partStatus       - The Participation status of the subject, or other participant on the appointment. Can be used to locate participants that have not responded to meeting requests.
     * @param patient          - One of the individuals of the appointment is this patient
     * @param practitioner     - One of the individuals of the appointment is this practitioner
     * @param serviceType      - The specific service that is to be performed during this appointment
     * @param status           - The overall status of the appointment
     * @return
     */
    @GetMapping(value = "/search", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Appointment> search(
            @RequestParam String actor,             //Appointment.participant.actor(Practitioner, Device, Patient, HealthcareService, RelatedPerson, Location)
            @RequestParam String appointmentType,   //Appointment.appointmentType
            @RequestParam String date,              //Appointment.start
            @RequestParam String identifier,        //Appointment.identifier
            @RequestParam String incomingReferral,  //Appointment.incomingReferral
            @RequestParam String location,          //Appointment.participant.actor(Location)
            @RequestParam String partStatus,        //Appointment.participant.status
            @RequestParam String patient,           //Appointment.participant.actor(Patient)
            @RequestParam String practitioner,      //Appointment.participant.actor(Practitioner)
            @RequestParam String serviceType,       //Appointment.Appointment.serviceType
            @RequestParam String status             //Appointment.status
    ) {
        //--- Demo search by parameter as Request Param ---//
        List<Appointment> appointments = appointmentService.listAll();

        return appointments;
    }

    /**
     * Lookup appointments
     * @param appointmentDto - search param
     * @return
     */
    @PostMapping(value = "/search", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Appointment> search(@RequestBody final Appointment searchAppointment) {
        //--- Search data by JSON Body as Condition ---//
        List<Appointment> appointments = appointmentService.lookup(searchAppointment);

        return appointments;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}
