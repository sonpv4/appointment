package com.fhir.demo;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.CodeSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Aspect
@Component
@Order(1)
public class HttpAspect {
	private final static Logger logger = LoggerFactory.getLogger(HttpAspect.class);

	// Execute before the monitoring method is executed
	//Define the pointcut here, use regular matching to match the specific method
	@Before("execution(public * com.fhir.demo.controller.*.*(..))")
	public void logBefore(final JoinPoint joinPoint) {

		ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
		HttpServletRequest request = attributes.getRequest();

		//This is the information to record the http request.
		logger.info("==============================================================>");

		//METHOD URL
		logger.info("{} {}", request.getMethod(), request.getRequestURL());

		//CLASS_METHOD
		logger.info("CLASS_METHOD = {}()", joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName());

		CodeSignature signature = (CodeSignature) joinPoint.getSignature();
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < joinPoint.getArgs().length; i++) {
			String parameterName = signature.getParameterNames()[i];
			builder.append(parameterName);
			builder.append(": ");
			builder.append(joinPoint.getArgs()[i].toString());
			builder.append(", ");
		}

		//ARGS
		logger.info("ARGS = {}", builder.toString());

		logger.info("<==============================================================");

	}


	// Get the return value of the monitoring method
	@AfterReturning(value = "execution(public * com.fhir.demo.controller.*.*(..))", returning = "object")
	public void logAfterReturn(final Object object) {

		if (object == null) {
			return;
		}

		//This is the information returned by the record http request.
		logger.info("==============================================================>");

		//RESPONSE
		logger.info("RESPONSE = {}", object.toString());

		logger.info("<==============================================================");

	}
}
