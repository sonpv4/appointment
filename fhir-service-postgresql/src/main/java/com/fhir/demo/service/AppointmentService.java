package com.fhir.demo.service;

import java.util.List;

import com.fhir.demo.model.Appointment;
import com.fhir.demo.model.CustomResource;
import org.springframework.web.bind.annotation.RequestParam;

public interface AppointmentService {

    /**
     * Create appointment
     *
     * @param appointment
     * @return
     */
    Appointment create(Appointment appointment);

    /**
     * Update appointment
     *
     * @param appointment
     * @return
     */
    Appointment update(Appointment appointment);

    /**
     * Lookup by id
     *
     * @param id
     * @return
     */
    CustomResource findById(long id);

    /**
     * Lookup appointment by id using native query
     *
     * @param id
     * @return
     */
    Appointment lookUpByAppointmentId(String id);

    /**
     * List all appointments
     *
     * @return
     */
    List<Appointment> listAll();

    /**
     * Lookup by id and version id
     *
     * @param id
     * @param versionId
     * @return
     */
    Appointment lookup(String id, String versionId);

    /**
     * Lookup appointments
     * @param actor - Any one of the individuals participating in the appointment
     * @param appointmentType - The style of appointment or patient that has been booked in the slot (not service type)
     * @param date - Appointment date/time
     * @param identifier - An Identifier of the Appointment
     * @param incomingReferral - The ReferralRequest provided as information to allocate to the Encounter
     * @param location - This location is listed in the participants of the appointment
     * @param partStatus - The Participation status of the subject, or other participant on the appointment. Can be used to locate participants that have not responded to meeting requests.
     * @param patient - One of the individuals of the appointment is this patient
     * @param practitioner - One of the individuals of the appointment is this practitioner
     * @param serviceType - The specific service that is to be performed during this appointment
     * @param status - The overall status of the appointment
     * @return
     */
    List<Appointment> lookup(String actor, String appointmentType, String date, String identifier, String incomingReferral, String location, String partStatus, String patient, String practitioner, String serviceType, String status);

    /**
     * Lookup appointments
     * @param appointment
     * @return
     */
    List<Appointment> lookup(Appointment appointment);

}
