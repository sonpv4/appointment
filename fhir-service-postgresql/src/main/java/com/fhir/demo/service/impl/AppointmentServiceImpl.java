package com.fhir.demo.service.impl;

import com.fhir.demo.model.Appointment;
import com.fhir.demo.model.CustomResource;
import com.fhir.demo.model.Meta;
import com.fhir.demo.repository.ResourceRepository;
import com.fhir.demo.service.AppointmentService;
import com.fhir.demo.util.JSONUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service("appointmentService")
public class AppointmentServiceImpl implements AppointmentService {

    @Autowired
    private ResourceRepository resourceRepository;

    @Override
    public Appointment create(final Appointment appointment) {
        Meta meta = appointment.getMeta();
        if (meta == null) {
            meta = new Meta();
        }
        meta.setVersionId("1");
        appointment.setMeta(meta);

        CustomResource customResource = new CustomResource();
        customResource.setContent(appointment);

        return resourceRepository.save(customResource).getContent();
    }

    public Appointment lookUpByAppointmentId(String id) {
        Appointment searchAppointment = new Appointment();
        searchAppointment.setId(id);
        Meta meta = new Meta();
        meta.setVersionId("1");
        searchAppointment.setMeta(meta);
        List<CustomResource> customResources = resourceRepository.findByCondition(JSONUtils.toJsonString(searchAppointment));
        if (customResources.isEmpty()) return null;
        return customResources.stream().map(o -> o.getContent()).collect(Collectors.toList()).get(0);
    }

    @Override
    public CustomResource findById(final long id) {
        return resourceRepository.findById(id);
    }

    @Override
    public List<Appointment> listAll() {
        Appointment searchAppointment = new Appointment();
        Meta meta = new Meta();
        meta.setVersionId("1");
        searchAppointment.setMeta(meta);
        String condition = JSONUtils.toJsonString(searchAppointment);
        System.out.println(condition);
        List<CustomResource> customResources = resourceRepository.findByCondition(condition);
        System.out.println(JSONUtils.toJsonString(searchAppointment));
        return customResources.stream().map(o -> o.getContent()).collect(Collectors.toList());
    }

    @Transactional
    @Override
    public Appointment update(final Appointment appointment) {
        Appointment searchAppointment = new Appointment();
        searchAppointment.setId(appointment.getId());
        Meta meta = new Meta();
        meta.setVersionId(appointment.getMeta().getVersionId());
        searchAppointment.setMeta(meta);
        List<CustomResource> customResources = resourceRepository.findByCondition(JSONUtils.toJsonString(searchAppointment));
        if (customResources.isEmpty()) {
            return null;
        }
        CustomResource customResource = customResources.get(0);
        Appointment existingAppointment = customResource.getContent();
        if (existingAppointment == null) {
            create(appointment);
        }
        appointment.getMeta().setVersionId(String.valueOf(Long.parseLong(existingAppointment.getMeta().getVersionId()) + 1));
        CustomResource newResource = new CustomResource();
        newResource.setContent(appointment);

        return resourceRepository.save(newResource).getContent();
    }

    @Override
    public Appointment lookup(final String id, final String versionId) {
        Appointment searchAppointment = new Appointment();
        searchAppointment.setId(id);
        Meta meta = new Meta();
        meta.setVersionId(versionId);
        searchAppointment.setMeta(meta);
        String condition = JSONUtils.toJsonString(searchAppointment);
        System.out.println(condition);
        List<CustomResource> customResources = resourceRepository.findByCondition(condition);
        if (!customResources.isEmpty()) {
            return customResources.get(0).getContent();
        }

        return null;
    }

    @Override
    public List<Appointment> lookup(String actor, String appointmentType, String date, String identifier, String incomingReferral, String location, String partStatus, String patient, String practitioner, String serviceType, String status) {
        return null;
    }

    @Override
    public List<Appointment> lookup(Appointment appointment) {
        Appointment searchAppointment = new Appointment();
        BeanUtils.copyProperties(appointment, searchAppointment, getNullPropertyNames(appointment));
        List<CustomResource> customResources = resourceRepository.findByCondition(JSONUtils.toJsonString(searchAppointment));

        return customResources.stream().map(CustomResource::getContent).collect(Collectors.toList());
    }

    /**
     * Returns an array of null properties of an object
     * @param source
     * @return
     */
    private String[] getNullPropertyNames (Object source) {
        final BeanWrapper src = new BeanWrapperImpl(source);
        java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();
        Set emptyNames = new HashSet();
        for(java.beans.PropertyDescriptor pd : pds) {
            //check if value of this property is null then add it to the collection
            Object srcValue = src.getPropertyValue(pd.getName());
            if (srcValue == null) emptyNames.add(pd.getName());
        }
        String[] result = new String[emptyNames.size()];
        return (String[]) emptyNames.toArray(result);
    }

}
