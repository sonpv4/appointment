package com.fhir.demo.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@ToString
public class Narrative {
	protected String status;
	protected String div;
}
