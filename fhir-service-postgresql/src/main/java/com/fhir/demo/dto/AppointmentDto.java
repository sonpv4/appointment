package com.fhir.demo.dto;

import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@ToString(callSuper = true)
@JsonInclude(Include.NON_NULL)
public class AppointmentDto extends DomainResourceDto {
	//	private String id;
	private List<IdentifierDto> identifier;
	private String status;
	private CodeableConceptDto cancelationReason;
	private List<CodeableConceptDto> serviceCategory;
	private List<CodeableConceptDto> serviceType;
	private List<CodeableConceptDto> specialty;
	private CodeableConceptDto appointmentType;
	private List<CodeableConceptDto> reasonCode;
	private int priority;
	private String description;
	private int minutesDuration;
	private List<ReferenceDto> slot;
	//	@JsonFormat(pattern = "dd/MM/yyyy hh:mm:ss a")
	//	private LocalDateTime created;
	private LocalDate created;
	private String comment;
	private List<ParticipantDto> participant;
	private List<PeriodDto> requestedPeriod;
}
