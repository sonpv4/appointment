package com.fhir.demo.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@ToString
@JsonInclude(Include.NON_NULL)
public class CodeableConceptDto {

	private List<CodingDto> coding;
	private String text;
}
