package com.fhir.demo.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@ToString
@JsonInclude(Include.NON_NULL)
public class IdentifierDto {

	private String use;
	private CodeableConceptDto type;
	private String system;
	private String value;
	private PeriodDto period;
}
