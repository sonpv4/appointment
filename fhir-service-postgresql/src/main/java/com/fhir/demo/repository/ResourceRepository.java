package com.fhir.demo.repository;

import com.fhir.demo.model.CustomResource;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ResourceRepository extends JpaRepository<CustomResource, Long> {

    CustomResource findById(long id);

    @Query(nativeQuery = true, value = "SELECT c.* FROM custom_resource c WHERE c.content @> CAST(:condition as jsonb)")
    List<CustomResource> findByCondition(@Param("condition") String parentOrderNumber);

    @Query(value = "SELECT c.* FROM custom_resource c WHERE c.content ->> 'id' = ?1", nativeQuery = true)
    List<CustomResource> findByAppointmentId(String id);

    List<CustomResource> findAll();
}
