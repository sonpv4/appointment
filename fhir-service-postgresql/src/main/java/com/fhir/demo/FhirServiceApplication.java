package com.fhir.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FhirServiceApplication {

	public static void main(final String[] args) {
		SpringApplication.run(FhirServiceApplication.class, args);
	}
}
