package com.fhir.demo.mapper;

import com.fhir.demo.dto.AppointmentDto;
import com.fhir.demo.model.Appointment;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import java.util.List;

@Mapper
public interface AppointmentMapper {
	AppointmentMapper INSTANCE = Mappers.getMapper(AppointmentMapper.class);

	/**
	 * AppointmentDto to Appointment
	 * @param appointmentDto
	 * @return
	 */
	Appointment toModel(AppointmentDto appointmentDto);

	/**
	 * Appointment to AppointmentDto
	 * @param appointment
	 * @return
	 */
	AppointmentDto toDto(Appointment appointment);

	/**
	 * Appointment list to AppointmentDto list
	 * @param appointment
	 * @return
	 */
	List<AppointmentDto> toDtos(List<Appointment> appointment);
}


