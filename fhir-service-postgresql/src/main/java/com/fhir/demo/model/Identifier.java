package com.fhir.demo.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class Identifier implements Serializable {

    private String use;
    private CodeableConcept type;
    private String system;
    private String value;
    private Period period;
}
