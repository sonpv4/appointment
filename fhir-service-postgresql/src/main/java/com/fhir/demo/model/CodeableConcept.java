package com.fhir.demo.model;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class CodeableConcept implements Serializable {

	private List<Coding> coding;
	private String text;
}
