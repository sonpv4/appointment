package com.fhir.demo.model;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Narrative {
	protected String status;
	protected String div;
}
