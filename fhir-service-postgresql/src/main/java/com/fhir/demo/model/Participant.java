package com.fhir.demo.model;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Participant implements Serializable {
	private List<CodeableConcept> type;
	private Reference actor;
	private String required;
	private String status;
	private Period period;
}
