package com.fhir.demo.model;

import java.time.LocalDateTime;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Meta {
	protected String versionId;
	protected LocalDateTime lastUpdated;;
	protected String source;
	protected List<Coding> security;
	protected List<Coding> tag;
}
