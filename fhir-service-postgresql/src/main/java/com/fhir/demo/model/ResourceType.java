package com.fhir.demo.model;

public enum ResourceType {
    APPOINTMENT, APPOINTMENT_HISTORY
}