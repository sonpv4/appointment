package com.fhir.demo.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter @Setter
public class Coding implements Serializable {
	private String system;
	private String version;
	private String code;
	private String display;
	private Boolean userSelected;
}
