package com.fhir.demo.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@Data
public class Appointment extends DomainResource implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotBlank(message = "id is mandatory")
    private String id;
    private List<Identifier> identifier;
    @NotBlank(message = "status is mandatory")
    private String status;
    private CodeableConcept cancelationReason;
    private List<CodeableConcept> serviceCategory;
    private List<CodeableConcept> serviceType;
    private List<CodeableConcept> specialty;
    private CodeableConcept appointmentType;
    private List<CodeableConcept> reasonCode;
    private Integer priority;
    private String description;
    private Integer minutesDuration;
    private List<Reference> slot;
    private LocalDate created;
    private String comment;
    private String start;
    private String end;
    private List<Participant> participant;
    private List<Period> requestedPeriod;
}
