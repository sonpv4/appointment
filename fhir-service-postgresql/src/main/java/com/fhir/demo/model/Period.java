package com.fhir.demo.model;

import java.io.Serializable;
import java.time.LocalDate;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Period implements Serializable {

	private LocalDate start;
	private LocalDate end;
}
