package com.fhir.demo.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter @Setter
public class Reference implements Serializable {
	private String reference;
	private String type;
	private Identifier identifier;
	private String display;
}
