package com.fhir.demo.model;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public abstract class Resource {

	//	@Id
	//	private String id;
	protected String resourceType;
	protected Meta meta;
	protected String implicitRules;
	protected String language;
}
