create table custom_resource
(
    id      SERIAL PRIMARY KEY,
    content jsonb NOT NULL
);